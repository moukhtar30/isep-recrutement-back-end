package com.example.isep.controller;

import com.example.isep.models.Candidat;
import com.example.isep.service.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CandidatController {
    @Autowired
    private CandidatService candidatService;

    @GetMapping("candidats")
    public List<Candidat> getAllCandidat(){
        return candidatService.getAllCandidat();
    }

    @GetMapping("candidats/{username}/{password}")
    public Candidat logCandidat(@PathVariable String username, @PathVariable String password){
        return candidatService.logCandidat(username,password);
    }

    @PostMapping("candidat")
    public Candidat addCandidat(@RequestBody @Valid Candidat candidat){
        return candidatService.addCandidat(candidat);
    }

    @PutMapping("candidat")
    public Candidat updateCandidat(@RequestBody @Valid Candidat candidat){
        return candidatService.updateCandidat(candidat);
    }

    @DeleteMapping("candidats/{id}")
    public void deleteCandidat(@PathVariable Long id){
        candidatService.deleteCandidat(id);
    }



}
