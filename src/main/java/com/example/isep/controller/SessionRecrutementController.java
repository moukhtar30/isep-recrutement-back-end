package com.example.isep.controller;

import com.example.isep.models.*;
import com.example.isep.service.SessionRecrutementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@RestController
@RequestMapping("sessionRecrutement")
public class SessionRecrutementController {
    @Autowired
    private SessionRecrutementService sessionRecrutementService;

    @GetMapping()
    public List<SessionRecrutement> getAllSessionRecrutement(){
        return sessionRecrutementService.getAllSessionRecrutement();
    }
    @GetMapping("/en-cours")
    public List<SessionRecrutement> getSessionsEnCours(){
        return sessionRecrutementService.getSessionsEnCours();
    }

    @GetMapping("/{id}")
    public SessionRecrutement getSessionRecrutementById(@PathVariable Long id){
        return sessionRecrutementService.getSessionRecrutementById(id);
    }
    /**
    @GetMapping("/poste/{id}")
    public SessionRecrutement getSessionRecrutementByPoste(@PathVariable Long id){
        return sessionRecrutementService.getSessionByPoste(id);
    }
     */
    @GetMapping("/poste/{id}")
    public List<SessionPoste>  getPostesBySession(@PathVariable Long id){
        return sessionRecrutementService.getPostesBySession(id);
    }
    @PostMapping
    public SessionRecrutement addSessionRecrutement(@RequestBody @Valid SessionRecrutement sessionRecrutement){
        return sessionRecrutementService.addSessionRecrutement(sessionRecrutement);
    }

    @PutMapping
    public SessionRecrutement updateSessionRecrutement(@RequestBody @Valid SessionRecrutement sessionRecrutement){
        return sessionRecrutementService.updateSessionRecrutement(sessionRecrutement);
    }

    @DeleteMapping("/{id}")
    public void deleteSessionRecrutement(@PathVariable Long id){
        sessionRecrutementService.deleteSessionRecrutement(id);
    }

    @PostMapping("/sessionPoste-add")
    public SessionPoste addSessionPoste(@RequestBody SessionPoste sessionPoste){
        return sessionRecrutementService.addSessionPoste(sessionPoste);
    }

    @PutMapping("/sessionPoste")
    public SessionPoste updateSessionPoste(@RequestBody SessionPoste sessionPoste){
        return sessionRecrutementService.updateSessionPoste(sessionPoste);
    }

    @DeleteMapping("/{idSeRe}/sessionPoste/poste/{idPoste}")
    public void deleteSessionPoste(@PathVariable Long idPoste, @PathVariable Long idSeRe){
        sessionRecrutementService.deleteSessionPoste(idPoste, idSeRe);
    }
    @DeleteMapping("/sessionPoste/poste/{idDoc}")
    public void deleteDocument(@PathVariable Long idDoc){
        sessionRecrutementService.deleteDocument(idDoc);
    }
    @PostMapping("/sessionPoste/dossierCandidature/{idDossierCand}/poste/{idPoste}/sessionRecrutement/{idSeRe}/dossierSessionPoste")
    public DossierSessionPoste addDossierSessionPoste(@PathVariable Long idDossierCand,
                                                      @PathVariable Long idPoste,
                                                      @PathVariable Long idSeRe,
                                                      @RequestBody DossierSessionPoste dossierSessionPoste){
       return sessionRecrutementService.addDossierSessionPoste(idDossierCand, idPoste, idSeRe, dossierSessionPoste);
    }

    @PutMapping("/dossierSessionPoste")
    public DossierSessionPoste updateDossierSessionPoste(@RequestBody DossierSessionPoste dossierSessionPoste){
        return sessionRecrutementService.updateDossierSessionPoste(dossierSessionPoste);
    }

    @PostMapping("/dossierSessionPoste/entretien")
    public Entretien addEntretien(@RequestBody Entretien entretien){
        return sessionRecrutementService.addEntretien(entretien);
    }

    @PutMapping("/dossierSessionPoste/entretien")
    public Entretien updateEntretien(@RequestBody Entretien entretien){
        return sessionRecrutementService.updateEntretien(entretien);
    }

    @DeleteMapping("dossierSessionPoste/entretien/{id}")
    public void deleteEntretien(Long id){
        sessionRecrutementService.deleteEntretien(id);
    }


}
