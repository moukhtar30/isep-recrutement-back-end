package com.example.isep.controller;

import com.example.isep.models.Commentaire;
import com.example.isep.models.Diplome;
import com.example.isep.models.DossierCandidature;
import com.example.isep.models.ExperiencePro;
import com.example.isep.service.DossierCandidatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("dossierCandidature")
public class DossierCandidatureController {
    @Autowired
    private DossierCandidatureService dossierCandidatureService;

    @PostMapping()
    public DossierCandidature addDossierCandidature(@RequestBody DossierCandidature dossierCandidature){
        return dossierCandidatureService.addDossierCandidature(dossierCandidature);
    }

    @PostMapping("/diplomes")
    public List<Diplome> addDiplome(@RequestBody List<Diplome> diplomes){
        return dossierCandidatureService.addDiplome(diplomes);
    }

    @PutMapping("/diplome")
    public Diplome updateDiplome(@RequestBody @Valid Diplome diplome){
        return dossierCandidatureService.updateDiplome(diplome);
    }

    @DeleteMapping("diplomes/{id}")
    public void deleteDiplome(@PathVariable Long id){
        dossierCandidatureService.deleteDiplome(id);
    }

    @PostMapping("/experiencePro")
    public List<ExperiencePro> addExperiencePro(@RequestBody @Valid List<ExperiencePro> experiencePros){
        return dossierCandidatureService.addExperiencePro(experiencePros);
    }

    @PutMapping("/experiencePro")
    public ExperiencePro updateExperiencePro(@RequestBody @Valid ExperiencePro experiencePro){
        return dossierCandidatureService.updateExperiencePro(experiencePro);
    }

    @DeleteMapping("/experiencePros/{id}")
    public void deleteExperiencePro(@PathVariable Long id){
        dossierCandidatureService.deleteExperiencePro(id);
    }

    @PostMapping("/{idDosCan}/commentaire")
    public Commentaire addCommentaire(@PathVariable Long idDosCan, @RequestBody Commentaire commentaire){
        return dossierCandidatureService.addCommentaire(idDosCan, commentaire) ;
    }

    @PutMapping("/commentaire")
    public Commentaire updateCommentaire(@RequestBody Commentaire commentaire){
        return dossierCandidatureService.updateCommentaire(commentaire);
    }

    @DeleteMapping("/commentaire/{id}")
    public void deleteCommentaire(@PathVariable Long id){
        dossierCandidatureService.deleteCommentaire(id);
    }





}
