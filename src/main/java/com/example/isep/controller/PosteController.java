package com.example.isep.controller;


import com.example.isep.models.DossierPertinent;
import com.example.isep.models.Poste;
import com.example.isep.service.PosteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("postes")
public class PosteController {

    @Autowired
    private PosteService posteService;


    @GetMapping()
    public List<Poste> getAllPoste(){
        return posteService.getAllPoste();
    }

    @PostMapping()
    public Poste addPoste(@RequestBody Poste poste){
        return posteService.addPoste(poste);
    }

    @PutMapping()
    public Poste updatePoste(@RequestBody Poste poste){
        return posteService.updatePoste(poste);
    }

    @GetMapping("/{id}")
    public Poste getPosteById(@PathVariable Long id){ return posteService.getPosteById(id);}
    
    @DeleteMapping("/{id}")
    public void deletePoste(@PathVariable Long id){
        posteService.deletePoste(id);
    }
    @GetMapping("/dossierPertinents")
    public List<DossierPertinent> getAllDossierPertinent(){
        return posteService.getAllDossierPertinent();
    }

    @GetMapping("/poste/{idPoste}/dossierCandidature/{idDosCan}/dossierPertinent")
    public DossierPertinent addDossierPertinent(@PathVariable Long idPoste,
                                                @PathVariable Long idDosCan,
                                                @RequestBody DossierPertinent dossierPertinent){
        return posteService.addDossierPertinent(idPoste, idDosCan, dossierPertinent);
    }

    @PutMapping("/dossierPertinent")
    public DossierPertinent updateDossierPertient(@RequestBody DossierPertinent dossierPertinent){
        return posteService.updateDossierPertient(dossierPertinent);
    }

    @DeleteMapping("dossierPertinent/dossierCandidat/{idDosCan}/poste/{idPoste}")
    public void deleteDossierPertinent(Long idDosCan, Long idPoste){
        posteService.deleteDossierPertinent(idDosCan, idPoste);
    }


}
