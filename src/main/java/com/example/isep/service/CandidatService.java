package com.example.isep.service;

import com.example.isep.models.Candidat;

import java.util.List;

public interface CandidatService {
    List<Candidat> getAllCandidat();
    Candidat addCandidat(Candidat candidat);
    Candidat updateCandidat(Candidat candidat);
    void deleteCandidat(Long id);
    Candidat logCandidat(String username, String password);
}
