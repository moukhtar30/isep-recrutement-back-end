package com.example.isep.service;

import com.example.isep.exception.ResourceNotFoundException;
import com.example.isep.models.Candidat;
import com.example.isep.repository.CandidatRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatServiceImpl implements CandidatService {
    @Autowired
    private CandidatRepository candidatRepository;



    @Override
    public List<Candidat> getAllCandidat() {
        return candidatRepository.findAll() ;
    }

    @Override
    public Candidat addCandidat(Candidat candidat) {
        return candidatRepository.save(candidat);
    }

    @Override
    public Candidat updateCandidat(Candidat candidat) {

        return candidatRepository.findById(candidat.getId())
                .map(cand-> candidatRepository.saveAndFlush(candidat))
                .orElseThrow(()-> new ResourceNotFoundException("le candidat n'existe pas"));
    }

    @Override
    public void deleteCandidat(Long id) {
        Candidat candidat = candidatRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("le Candidat que vous voulez supprimer n'existe pas ") );
        candidatRepository.delete(candidat);
    }

    @Override
    public Candidat logCandidat(String username, String password) {
       Candidat candidat = candidatRepository.findByUsernameAndPassword(username,password);
       if (candidat == null) {
       }
        return candidat;
    }

}
