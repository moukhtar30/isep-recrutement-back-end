package com.example.isep.service;

import com.example.isep.models.DossierPertinent;
import com.example.isep.models.Poste;

import java.util.List;

public interface PosteService {
    List<Poste> getAllPoste();
    Poste addPoste(Poste poste);
    Poste updatePoste(Poste poste);
    void deletePoste(Long id);
    List<DossierPertinent> getAllDossierPertinent();
    DossierPertinent addDossierPertinent(Long idPoste, Long idDosCan, DossierPertinent dossierPertinent);
    DossierPertinent updateDossierPertient(DossierPertinent dossierPertinent);
    void deleteDossierPertinent(Long idDosCan, Long idPoste);
    Poste getPosteById(Long id);
}

