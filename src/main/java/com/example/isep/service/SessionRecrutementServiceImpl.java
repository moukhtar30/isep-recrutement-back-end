package com.example.isep.service;

import com.example.isep.exception.ResourceNotFoundException;
import com.example.isep.models.*;
import com.example.isep.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SessionRecrutementServiceImpl implements SessionRecrutementService {
    @Autowired
    private SessionRecrutementRepository sessionRecrutementRepository;
    @Autowired
    private SessionPosteRepository sessionPosteRepository;
    @Autowired
    private PosteRepository posteRepository;
    @Autowired
    private DossierCandidatureRepository dossierCandidatureRepository;
    @Autowired
    private DocumentAFournirRepository documentAFournirRepository;
    @Autowired
    private DossierSessionPosteRepository dossierSessionPosteRepository;
    @Autowired
    private EntretienRepository entretienRepository;
    @Autowired
    MediaService mediaService;


    @Override
    public List<SessionRecrutement> getAllSessionRecrutement() {
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        System.out.println(sessionRecrutementRepository.findAll());
        return sessionRecrutementRepository.findAll();
    }

    @Override
    public SessionRecrutement getSessionRecrutementById(Long id) {
        return sessionRecrutementRepository.findById(id)
                .map(sessionRecrutement -> sessionRecrutement)
                .orElseThrow(()-> new ResourceNotFoundException("Info session recrutement indisponnible"));
    }

    @Override
    public SessionRecrutement addSessionRecrutement(SessionRecrutement sessionRecrutement) {
       /* List<SessionPoste> sessionPostes = new ArrayList<>(sessionRecrutement.getSessionPostes());
        sessionRecrutement.setSessionPostes(new ArrayList<>());*/
        SessionRecrutement sR = sessionRecrutementRepository.save(sessionRecrutement);
       /* for(SessionPoste sessionPoste: sessionPostes){
           sessionPoste.setDateCreation(LocalDateTime.now());
            sessionPoste.getPk().setSessionRecrutement(sR);
            sessionPosteRepository.save(sessionPoste);
            List<DocumentAFournir> documentAFournirs = new ArrayList<>(sessionPoste.getDocumentAFournirs());
            sessionPoste.setDocumentAFournirs(new ArrayList<>());
            for (DocumentAFournir documentAFournir: documentAFournirs){
                documentAFournir.setSessionPoste(sessionPoste);
                documentAFournirRepository.save(documentAFournir);
            }
        }*/
        return sR;

    }

    @Override
    public SessionRecrutement updateSessionRecrutement(SessionRecrutement sessionRecrutement) {
        sessionRecrutementRepository.findById(sessionRecrutement.getId())
                .orElseThrow(()-> new ResourceNotFoundException("la session de recrutement n'existe pas"));
       /** List<SessionPoste> sessionPostes = new ArrayList<>(sessionRecrutement.getSessionPostes());
        sessionRecrutement.setSessionPostes(new ArrayList<>());
        for(SessionPoste sP: sessionPostes){
            sP.setDateCreation(LocalDateTime.now());
            sP.getPk().setSessionRecrutement(sessionRecrutement);
            sessionPosteRepository.save(sP);
            if(sP.getDocumentAFournirs() != null) {
                List<DocumentAFournir> docs = new ArrayList<>(sP.getDocumentAFournirs());
                sP.setDocumentAFournirs(new ArrayList<>());
                for (DocumentAFournir doc : docs) {
                    doc.setSessionPoste(sP);
                    documentAFournirRepository.save(doc);
                }
            }
        }*/
        return sessionRecrutementRepository.save(sessionRecrutement);
    }

    @Override
    public void deleteSessionRecrutement(Long id) {
        SessionRecrutement sessionRecrutement = sessionRecrutementRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("la session de recrutement n'existe pas"));
        sessionRecrutementRepository.delete(sessionRecrutement);
    }

    @Override
    public SessionPoste addSessionPoste(SessionPoste sessionPoste) {
       /** Poste poste = posteRepository.findById(idPoste)
                            .orElseThrow(() -> new ResourceNotFoundException("Poste non disponnoble"));
        SessionRecrutement sessionRecrutement = sessionRecrutementRepository.findById(idSessionRecru)
                            .orElseThrow(() -> new ResourceNotFoundException("session recrutement non disponnible"));
        SessionPostePk sessionPostePk = new SessionPostePk();
        sessionPostePk.setPoste(poste);
        sessionPostePk.setSessionRecrutement(sessionRecrutement);
        SessionPoste sessionPoste = new SessionPoste();
        sessionPoste.setPk(sessionPostePk);
        LocalDateTime dateCreation = LocalDateTime.now();
        sessionPoste.setDateCreation(dateCreation);
        SessionPoste sessionPosteTmp = sessionPosteRepository.save(sessionPoste);
        documentAFournir.setSessionPoste(sessionPosteTmp);
        documentAFournirRepository.save(documentAFournir);
        return sessionPosteTmp;
        */
       if(sessionPoste.getDocumentAFournirs() != null){
           List<DocumentAFournir> documentAFournirs = new ArrayList<>(sessionPoste.getDocumentAFournirs());
           sessionPoste.setDocumentAFournirs(new ArrayList<>());
           for (DocumentAFournir documentAFournir: documentAFournirs){
               documentAFournir.setSessionPoste(sessionPoste);
               documentAFournirRepository.save(documentAFournir);
           }
       }
       sessionPoste.setDateCreation(LocalDateTime.now());
       return sessionPosteRepository.save(sessionPoste);
    }

    @Override
    public SessionPoste updateSessionPoste(SessionPoste sessionPoste) {
        sessionPosteRepository.findById(sessionPoste.getPk())
                .orElseThrow(()-> new ResourceNotFoundException("Ce session poste n'existe pas"));
        if(sessionPoste.getDocumentAFournirs() != null) {
            List<DocumentAFournir> docs = new ArrayList<>(sessionPoste.getDocumentAFournirs());
            sessionPoste.setDocumentAFournirs(new ArrayList<>());
            for (DocumentAFournir doc : docs) {
                doc.setSessionPoste(sessionPoste);
                documentAFournirRepository.save(doc);
            }
        }
        return sessionPosteRepository.saveAndFlush(sessionPoste);
    }

    @Override
    public void deleteSessionPoste(Long idPoste, Long idSeRe) {
        Poste poste = posteRepository.findById(idPoste)
                .orElseThrow(()-> new ResourceNotFoundException("le poste n'existe pas"));
        SessionRecrutement sessionRecrutement = sessionRecrutementRepository.findById(idSeRe)
                .orElseThrow(()-> new ResourceNotFoundException("la session de recrutement n'existe pas"));
        SessionPostePk pk = new SessionPostePk();
        pk.setPoste(poste);
        pk.setSessionRecrutement(sessionRecrutement);
        SessionPoste sessionPoste = sessionPosteRepository.findById(pk)
                .orElseThrow(()-> new ResourceNotFoundException("la session de poste n'existe pas"));
        sessionPosteRepository.delete(sessionPoste);
    }
    @Override
    public SessionRecrutement getSessionByPoste(Long idPoste){
        Poste poste = posteRepository.findById(idPoste)
                .orElseThrow(()-> new ResourceNotFoundException("Ce poste n'existe pas"));
        SessionPostePk pk = new SessionPostePk();
        pk.setPoste(poste);
        /**
        SessionPoste sessionPoste = sessionPosteRepository.findByPk(pk);
        sessionPoste.getPk().getSessionRecrutement()*/
        return pk.getSessionRecrutement();
    }

    @Override
    public List<SessionPoste> getPostesBySession(Long idSession) {
        SessionRecrutement sessionRecrutement = sessionRecrutementRepository.findById(idSession)
                .orElseThrow(()-> new ResourceNotFoundException("La session demandée n'existe pas"));
        return sessionRecrutement.getSessionPostes();
    }

    @Override
    public void deleteDocument(Long idDoc) {
        DocumentAFournir documentAFournir = documentAFournirRepository.findById(idDoc)
                .orElseThrow(()-> new ResourceNotFoundException("le document que vous voulez supprimer, n'existe pas"));
        documentAFournirRepository.delete(documentAFournir);
    }

    @Override
    public List<SessionRecrutement> getSessionsEnCours() {
        return sessionRecrutementRepository.findAllByDateDebutBeforeAndDateFinAfter(LocalDateTime.now(),LocalDateTime.now());
    }

    @Override
    public DossierSessionPoste addDossierSessionPoste(Long idDossierCand, Long idPoste, Long idSeRe, DossierSessionPoste dossierSessionPoste) {
        DossierCandidature dossierCandidature = dossierCandidatureRepository.findById(idDossierCand)
                .orElseThrow(()-> new ResourceNotFoundException("le dossier de candidature n'existe pas"));

        Poste poste = posteRepository.findById(idPoste)
                .orElseThrow(()-> new ResourceNotFoundException("le poste n'existe pas"));

        SessionRecrutement sessionRecrutement = sessionRecrutementRepository.findById(idPoste)
                .orElseThrow(()-> new ResourceNotFoundException("la session de recrutement n'existe pas"));

        SessionPostePk pk = new SessionPostePk();
        pk.setPoste(poste);
        pk.setSessionRecrutement(sessionRecrutement);

        SessionPoste sessionPoste = sessionPosteRepository.findById(pk)
                .orElseThrow(()-> new ResourceNotFoundException("la session poste n'existe pas"));

        DossierSessionPostePk dossierSessionPostePk = new DossierSessionPostePk();
        dossierSessionPostePk.setDossierCandidature(dossierCandidature);
        dossierSessionPostePk.setPoste(sessionPoste);
        dossierSessionPoste.setPk(dossierSessionPostePk);
        List<PieceJointe> pieceJointes = dossierSessionPoste.getPieceJointes();
        dossierSessionPoste.setPieceJointes(null);
        DossierSessionPoste dossierSessionPosteTmp = dossierSessionPosteRepository.save(dossierSessionPoste);
        if (pieceJointes.size() != 0){
        for (PieceJointe piece : pieceJointes){
            piece.setDossierSessionPoste(dossierSessionPosteTmp);
        }
        pieceJointes = mediaService.createMedia(pieceJointes);
        dossierSessionPosteTmp.setPieceJointes(pieceJointes);
        }
        return dossierSessionPosteTmp;
    }

    @Override
    public DossierSessionPoste updateDossierSessionPoste(DossierSessionPoste dossierSessionPoste) {

        return dossierSessionPosteRepository.findById(dossierSessionPoste.getPk())
                .map(dossierSessionPosteTmp -> dossierSessionPosteRepository.saveAndFlush(dossierSessionPoste) )
                .orElseThrow(()-> new ResourceNotFoundException("la session de poste n'existe pas"));
    }



    @Override
    public Entretien addEntretien(Entretien entretien) {

        return dossierSessionPosteRepository.findById(entretien.getDossierSessionPoste().getPk())
                .map(dossierSessionPoste -> {
                    System.out.println("le dossier existe");
                    return entretienRepository.save(entretien);
                })
                .orElseThrow(()-> new ResourceNotFoundException("Dossier session de poste indisponnible"));
    }

    @Override
    public Entretien updateEntretien(Entretien entretien) {

        return entretienRepository.findById(entretien.getId())
                .map(entretienTmp -> entretienRepository.saveAndFlush(entretien))
                .orElseThrow(()-> new ResourceNotFoundException("Entretien n'existe pas"));
    }

    @Override
    public void deleteEntretien(Long id) {
        Entretien entretien = entretienRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("l'entretien n'existe pas"));
        entretienRepository.delete(entretien);
    }


}
