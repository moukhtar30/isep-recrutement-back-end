package com.example.isep.service;


import com.example.isep.models.PieceJointe;

import java.util.List;

public interface MediaService {
    PieceJointe getMediaById(Long id);
    PieceJointe saveMedia(PieceJointe pieceJointe);
    PieceJointe updateMedia(PieceJointe pieceJointe);
    List<PieceJointe> createMedia(List<PieceJointe> pieceJointes);
    void deleteMedia(Long id);
    List<PieceJointe> getAllMedias();
}
