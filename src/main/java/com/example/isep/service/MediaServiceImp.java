package com.example.isep.service;

import com.example.isep.exception.ResourceNotFoundException;
import com.example.isep.models.PieceJointe;
import com.example.isep.repository.MediaRepository;
import com.example.isep.util.Base64EncodeDecode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MediaServiceImp implements MediaService {
    @Autowired
    private MediaRepository mediaRepository;
    @Value("${pathFile}")
    private String pathFile;
    @Value("${source}")
    private String sourceFile;

    @Override
    public PieceJointe getMediaById(Long id) {

        PieceJointe pieceJointe = mediaRepository.getOne(id);
        if (pieceJointe == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return mediaRepository.getOne(id);
    }

    @Override
    public PieceJointe saveMedia(PieceJointe pieceJointe) {
        return mediaRepository.save(pieceJointe);
    }

    @Override
    public PieceJointe updateMedia(PieceJointe pieceJointe) {
        return mediaRepository.saveAndFlush(pieceJointe);
    }

    @Override
    public void deleteMedia(Long id) {
        PieceJointe pieceJointe = mediaRepository.getOne(id);
        if (pieceJointe == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        mediaRepository.deleteById(id);
    }

    @Override
    public List<PieceJointe> getAllMedias() {
        return mediaRepository.findAll();
    }


    public List<PieceJointe> createMedia(List<PieceJointe> pieceJointes) {
        List<PieceJointe> medias2 = new ArrayList<>();
        for (PieceJointe pieceJointe : pieceJointes) {
            File dossier = new File("C:/wamp/www/pieceJointes/");
            if (!dossier.exists()) {
                dossier.mkdir();
            }
            if (!dossier.isDirectory()) {
                dossier.mkdir();
            }

            Date date = new Date();
            long currentTime = date.getTime();

            String source = +currentTime + ".jpeg";

            if (Base64EncodeDecode.decoder(pieceJointe.getUrl(), pathFile + currentTime + "." + pieceJointe.getType())) {
                pieceJointe.setUrl("/pieceJointes/" + currentTime + "." + pieceJointe.getType());


            }
            pieceJointe = mediaRepository.save(pieceJointe);
            medias2.add(pieceJointe);
            //return null;
        }
        return medias2;
    }
}