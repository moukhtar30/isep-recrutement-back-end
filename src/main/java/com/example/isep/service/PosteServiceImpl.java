package com.example.isep.service;

import com.example.isep.exception.ResourceNotFoundException;
import com.example.isep.models.DossierCandidature;
import com.example.isep.models.DossierPertinent;
import com.example.isep.models.DossierPertinentPk;
import com.example.isep.models.Poste;
import com.example.isep.repository.DossierCandidatureRepository;
import com.example.isep.repository.DossierPertinentRepository;
import com.example.isep.repository.PosteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PosteServiceImpl implements PosteService {

    @Autowired
    private PosteRepository posteRepository;
    @Autowired
    private DossierPertinentRepository dossierPertinentRepository;
    @Autowired
    private DossierCandidatureRepository dossierCandidatureRepository;



    @Override
    public List<Poste> getAllPoste() {
        return posteRepository.findAll();
    }

    @Override
    public Poste addPoste(Poste poste) {
        return posteRepository.save(poste) ;
    }

    @Override
    public Poste updatePoste(Poste poste) {
        return posteRepository.findById(poste.getId())
                .map(posteTmp -> posteRepository.saveAndFlush(poste))
                .orElseThrow(()-> new ResourceNotFoundException("Le poste n'existe pas"));
    }

    @Override
    public void deletePoste(Long id) {
        Poste poste = posteRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("le poste n'existe pas"));
        posteRepository.delete(poste);
    }

    @Override
    public List<DossierPertinent> getAllDossierPertinent() {
        return dossierPertinentRepository.findAll();
    }

    @Override
    public DossierPertinent addDossierPertinent(Long idPoste, Long idDosCan, DossierPertinent dossierPertinent) {

        Poste poste = posteRepository.findById(idPoste)
                .orElseThrow(()-> new ResourceNotFoundException("le Poste n'existe pas"));
        DossierCandidature dossierCandidature = dossierCandidatureRepository.findById(idDosCan)
                .orElseThrow(()-> new ResourceNotFoundException("le Dossier candidature n'existe pas"));
        DossierPertinentPk pk = new DossierPertinentPk();
        pk.setPoste(poste);
        pk.setDossierCandidature(dossierCandidature);
        dossierPertinent.setPk(pk);
        return dossierPertinentRepository.save(dossierPertinent);
    }

    @Override
    public DossierPertinent updateDossierPertient(DossierPertinent dossierPertinent) {
        return dossierPertinentRepository.findById(dossierPertinent.getPk())
                .map(dossierPertinentTmp -> dossierPertinentRepository.save(dossierPertinent) )
                .orElseThrow(()-> new ResourceNotFoundException("Dossier pertinent non disponnible"));
    }

    @Override
    public void deleteDossierPertinent(Long idDosCan, Long idPoste) {
        Poste poste = posteRepository.findById(idPoste)
                .orElseThrow(()-> new ResourceNotFoundException("le Poste n'existe pas"));
        DossierCandidature dossierCandidature = dossierCandidatureRepository.findById(idDosCan)
                .orElseThrow(()-> new ResourceNotFoundException("le Dossier candidature n'existe pas"));
        DossierPertinentPk pk = new DossierPertinentPk();
        pk.setPoste(poste);
        pk.setDossierCandidature(dossierCandidature);
        DossierPertinent dossierPertinent = dossierPertinentRepository.findById(pk)
                .orElseThrow(()-> new ResourceNotFoundException("le dossier pertinent n'existe pas"));
        dossierPertinentRepository.delete(dossierPertinent);

    }

    @Override
    public Poste getPosteById(Long id) {
        Poste poste = posteRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Ce poste n'existe pas"));
        return poste;
    }

}
