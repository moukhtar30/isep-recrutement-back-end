package com.example.isep.service;

import com.example.isep.models.Commentaire;
import com.example.isep.models.Diplome;
import com.example.isep.models.DossierCandidature;
import com.example.isep.models.ExperiencePro;

import java.util.List;

public interface DossierCandidatureService {
    DossierCandidature addDossierCandidature(DossierCandidature dossierCandidature);
    List<Diplome> addDiplome(List<Diplome> diplomes);
    Diplome updateDiplome(Diplome diplome);
    void deleteDiplome(Long id);
    List<ExperiencePro> addExperiencePro(List<ExperiencePro> experiencePros);
    ExperiencePro updateExperiencePro(ExperiencePro experiencePro);
    void deleteExperiencePro(Long id);
    Commentaire addCommentaire(Long idDosCan, Commentaire commentaire);
    Commentaire updateCommentaire(Commentaire commentaire);
    void deleteCommentaire(Long id);


}
