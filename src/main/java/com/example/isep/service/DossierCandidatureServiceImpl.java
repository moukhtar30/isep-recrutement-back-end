package com.example.isep.service;

import com.example.isep.exception.ResourceNotFoundException;
import com.example.isep.models.Commentaire;
import com.example.isep.models.Diplome;
import com.example.isep.models.DossierCandidature;
import com.example.isep.models.ExperiencePro;
import com.example.isep.repository.CommentaireRepository;
import com.example.isep.repository.DiplomeRepository;
import com.example.isep.repository.DossierCandidatureRepository;
import com.example.isep.repository.ExperienceProRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DossierCandidatureServiceImpl implements DossierCandidatureService {
    @Autowired
    private ExperienceProRepository experienceProRepository;
    @Autowired
    private DiplomeRepository diplomeRepository;
    @Autowired
    private CommentaireRepository commentaireRepository;
    @Autowired
    private DossierCandidatureRepository dossierCandidatureRepository;



    @Override
    public List<Diplome> addDiplome(List<Diplome> diplomes) {
        return diplomeRepository.saveAll(diplomes);
    }

    @Override
    public Diplome updateDiplome(Diplome diplome) {
        return diplomeRepository.findById(diplome.getId())
                .map(diplo -> diplomeRepository.saveAndFlush(diplome))
                .orElseThrow(()-> new ResourceNotFoundException("le diplome que vous voulez update n'existe pas"));
    }

    @Override
    public void deleteDiplome(Long id) {
        Diplome diplome = diplomeRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("le diplome n'existe pas"));
        diplomeRepository.delete(diplome);
    }

    @Override
    public List<ExperiencePro> addExperiencePro(List<ExperiencePro> experiencePros) {
        return experienceProRepository.saveAll(experiencePros);
    }

    @Override
    public ExperiencePro updateExperiencePro(ExperiencePro experiencePro) {
        return experienceProRepository.findById(experiencePro.getId())
                .map(experience -> experienceProRepository.saveAndFlush(experience))
                .orElseThrow(()-> new ResourceNotFoundException("l'experience n'existe pas"));
    }

    @Override
    public void deleteExperiencePro(Long id) {
        ExperiencePro experiencePro = experienceProRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("l'expression que vous voulez supprimer n'existe pas"));
        experienceProRepository.delete(experiencePro);
    }

    @Override
    public Commentaire addCommentaire(Long idDosCan, Commentaire commentaire) {
        return  dossierCandidatureRepository.findById(idDosCan)
                .map(dossierCandidature -> {
                    commentaire.setDossierCandidature(dossierCandidature);
                    return commentaireRepository.save(commentaire);
                })
                .orElseThrow(()-> new ResourceNotFoundException("le Dossier de Candiature n'existe pas"));
    }

    @Override
    public Commentaire updateCommentaire(Commentaire commentaire) {

        return commentaireRepository.findById(commentaire.getId())
                .map(comment -> commentaireRepository.saveAndFlush(commentaire))
                .orElseThrow(()-> new ResourceNotFoundException("update impossible sur commentaire"));
    }

    @Override
    public void deleteCommentaire(Long id) {
        Commentaire commentaire = commentaireRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("suppression impossible commentaire does not existe"));
        commentaireRepository.delete(commentaire);
    }


    @Override
    public DossierCandidature addDossierCandidature(DossierCandidature dossierCandidature) {
        return dossierCandidatureRepository.save(dossierCandidature);
    }
}
