package com.example.isep.service;

import com.example.isep.models.*;

import java.util.List;

public interface SessionRecrutementService {
    List<SessionRecrutement> getAllSessionRecrutement();
    SessionRecrutement getSessionRecrutementById(Long id);
    SessionRecrutement addSessionRecrutement(SessionRecrutement sessionRecrutement);
    SessionRecrutement updateSessionRecrutement(SessionRecrutement sessionRecrutement);
    void deleteSessionRecrutement(Long id);
    SessionPoste addSessionPoste(SessionPoste sessionPoste);
    SessionPoste updateSessionPoste(SessionPoste sessionPoste);
    void deleteSessionPoste(Long idPoste, Long idSeRe);
    DossierSessionPoste addDossierSessionPoste(Long idDossierCand, Long idPoste, Long idSeRe, DossierSessionPoste dossierSessionPoste);
    DossierSessionPoste updateDossierSessionPoste(DossierSessionPoste dossierSessionPoste);
    Entretien addEntretien(Entretien entretien);
    Entretien updateEntretien(Entretien entretien);
    void deleteEntretien(Long id);
    SessionRecrutement getSessionByPoste(Long idPoste);
    List<SessionPoste> getPostesBySession(Long idSession);
    void deleteDocument(Long idDoc);
    List<SessionRecrutement> getSessionsEnCours();

}
