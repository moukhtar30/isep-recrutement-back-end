package com.example.isep.models;


import com.example.isep.util.CustomCheminSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Getter
@Setter
public class PieceJointe implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private String nom;
    @NotNull(message = "Veuillez renseigner l'url du media")
    @JsonSerialize(using = CustomCheminSerializer.class)
    @Column(name="url", columnDefinition="TEXT")
    private String url;
    private String type;

    @ManyToOne
    private DossierSessionPoste dossierSessionPoste;
}
