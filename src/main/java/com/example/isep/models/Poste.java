package com.example.isep.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
public class Poste implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String nom;
    @OneToMany(mappedBy = "pk.poste")
    @JsonIgnore
    private List<SessionPoste> sessionPostes;
}
