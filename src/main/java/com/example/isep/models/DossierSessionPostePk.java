package com.example.isep.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Embeddable
@Data
public class DossierSessionPostePk implements Serializable {
    @ManyToOne
    @JsonIgnore
    private DossierCandidature dossierCandidature;
    @ManyToOne
    private SessionPoste poste;
}
