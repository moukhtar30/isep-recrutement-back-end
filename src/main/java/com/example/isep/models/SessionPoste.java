package com.example.isep.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class SessionPoste implements Serializable {
    private LocalDateTime dateCreation;
    @EmbeddedId
    private SessionPostePk pk;
    @OneToMany(mappedBy = "sessionPoste")
    private List<DocumentAFournir> documentAFournirs;
}
