package com.example.isep.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class SessionPostePk implements Serializable {
    @ManyToOne
    private SessionRecrutement sessionRecrutement;
    @ManyToOne
    private Poste poste;



}
