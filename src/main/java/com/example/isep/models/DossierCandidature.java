package com.example.isep.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
@Entity
@Data
public class DossierCandidature implements Serializable {
    @GeneratedValue
    @Id
    @NotNull
    private Long id;
    @NotNull
    private String etat;
    @OneToOne
    private Candidat candidat;
    @OneToMany(mappedBy = "dossierCandidature")
    private List<ExperiencePro> experiencePros;
    @OneToMany(mappedBy = "dossierCandidature")
    private List<Diplome> diplomes;
    @OneToMany(mappedBy = "dossierCandidature")
    private List<Commentaire> commentaires;
}
