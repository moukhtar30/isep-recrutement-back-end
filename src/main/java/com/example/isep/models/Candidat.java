package com.example.isep.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
public class Candidat implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String prenom;
    @NotNull
    private String nom;
    @NotNull
    private String email;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @JsonIgnore
    @OneToOne(mappedBy = "candidat")
    private DossierCandidature dossierCandidatures;
}
