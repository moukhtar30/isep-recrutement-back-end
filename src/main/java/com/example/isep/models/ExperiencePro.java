package com.example.isep.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
@Entity
@Data
public class ExperiencePro implements Serializable {
    @GeneratedValue
    @Id
    @NotNull
    private Long id;
    @NotNull
    private String posteOccupe;
    private Date dateDebut;
    @NotNull
    private String entreprise;
    private String description;
    private Date datefin;
    @ManyToOne
    private DossierCandidature dossierCandidature;
}
