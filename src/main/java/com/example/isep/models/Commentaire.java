package com.example.isep.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Entity
@Data
public class Commentaire implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String corps;
    @ManyToOne
    private DossierCandidature dossierCandidature;
}
