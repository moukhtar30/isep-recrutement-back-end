package com.example.isep.models;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;
@Entity
@Data
public class DossierSessionPoste implements Serializable {
    @EmbeddedId
    private DossierSessionPostePk pk;
    @OneToMany (mappedBy = "dossierSessionPoste")
    private List<PieceJointe> pieceJointes;
    @OneToMany (mappedBy = "dossierSessionPoste")
    private List<Entretien> entretiens;
}
