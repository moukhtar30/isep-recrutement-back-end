package com.example.isep.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Entity
@Data
public class Diplome implements Serializable {
    @GeneratedValue
    @Id
    private Long id;
    @NotNull
    private String nom;
    @NotNull
    private String anneeObtention;
    @NotNull
    private String structure;
    @ManyToOne
    @JsonIgnore
    private DossierCandidature dossierCandidature;
}
