package com.example.isep.models;

import com.sun.istack.internal.NotNull;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class DossierPertinent implements Serializable {
    @NotNull
    private Date dateCreation;
    @NotNull
    private String observation;
    @EmbeddedId
    private DossierPertinentPk pk;
}
