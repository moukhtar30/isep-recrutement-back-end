package com.example.isep.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Entity
@Data
public class Entretien implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private DossierSessionPoste dossierSessionPoste;
}
