package com.example.isep.repository;

import com.example.isep.models.DocumentAFournir;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentAFournirRepository extends JpaRepository<DocumentAFournir, Long> {
}
