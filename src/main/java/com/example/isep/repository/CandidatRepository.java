package com.example.isep.repository;

import com.example.isep.models.Candidat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidatRepository extends JpaRepository<Candidat, Long> {
    Candidat findByUsernameAndPassword(String username, String password);
}
