package com.example.isep.repository;

import com.example.isep.models.DossierPertinent;
import com.example.isep.models.DossierPertinentPk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DossierPertinentRepository extends JpaRepository<DossierPertinent, DossierPertinentPk> {
}
