package com.example.isep.repository;

import com.example.isep.models.PieceJointe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PieceJointeRepository extends JpaRepository<PieceJointe, Long> {
}
