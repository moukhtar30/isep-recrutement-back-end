package com.example.isep.repository;

import com.example.isep.models.ExperiencePro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperienceProRepository extends JpaRepository<ExperiencePro, Long> {
}
