package com.example.isep.repository;

import com.example.isep.models.SessionRecrutement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface SessionRecrutementRepository extends JpaRepository<SessionRecrutement, Long> {
   List <SessionRecrutement> findAllByDateDebutBeforeAndDateFinAfter(LocalDateTime localDateTime, LocalDateTime localDateTime1);
}
