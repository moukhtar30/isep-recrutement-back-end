package com.example.isep.repository;

import com.example.isep.models.SessionPoste;
import com.example.isep.models.SessionPostePk;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SessionPosteRepository extends JpaRepository<SessionPoste, SessionPostePk> {
    SessionPoste findByPk(SessionPostePk pk);
}
