package com.example.isep.repository;

import com.example.isep.models.DossierCandidature;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DossierCandidatureRepository extends JpaRepository<DossierCandidature, Long> {
}
