package com.example.isep.repository;

import com.example.isep.models.DossierSessionPoste;
import com.example.isep.models.DossierSessionPostePk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DossierSessionPosteRepository extends JpaRepository<DossierSessionPoste, DossierSessionPostePk> {
}
