package com.example.isep.repository;

import com.example.isep.models.Diplome;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiplomeRepository extends JpaRepository<Diplome, Long> {
}
