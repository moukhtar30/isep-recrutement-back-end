package com.example.isep.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;


public class CustomDateDeserializer extends StdDeserializer<LocalDateTime> {

    protected CustomDateDeserializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Object object=jsonParser.readValueAsTree();
        if(object!=null && object instanceof LongNode){
            return  Instant.ofEpochMilli(((LongNode) object).longValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        if(object!=null && object instanceof TextNode){
            return  Instant.parse(((TextNode) object).textValue()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        return null;
    }
}
